package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RedBlackLiteBST <Key extends Comparable<Key>, Value extends Comparable<Value>>{
    
	private static final boolean RED   = true;
    private static final boolean BLACK = false;
    private int alturas=0;

    private Nodo raiz;    
	private class Nodo {
		private Key key;
		private Value val;
		private Nodo izq, der;
		private boolean color;
		private int size;
		
		public Nodo(Key keyy, Value vall, boolean col, int s)
		{
			this.key=keyy;
			this.val=vall;
			this.color=col;
			this.size=s;
			
		}
	}
	//Put
	
	private boolean isRed (Nodo x)
	{
		if(x==null) return false;
		return x.color==RED;
	}
	
	private int size(Nodo x)
	{
		if(x==null) return 0;
		return x.size;
	}
	

	public void put(Key key, Value val)
	{
		if(key==null) throw new IllegalArgumentException("primer argumento es nulo");
		if(val==null)
		{
			return;
		}
		raiz=insert(raiz, key, val);
		raiz.color=BLACK;
	}
	
	private Nodo insert(Nodo h, Key key, Value val)
	{
		if(h==null) return new Nodo(key, val, RED, 1);
		 int cmp = key.compareTo(h.key);
	        if      (cmp < 0) h.izq  = insert(h.izq,  key, val); 
	        else if (cmp > 0) h.der = insert(h.der, key, val); 
	        else              h.val   = val;
	        if (isRed(h.der) && !isRed(h.izq))      h = rotateLeft(h);
	        if (isRed(h.izq)  &&  isRed(h.izq.izq)) h = rotateRight(h);
	        if (isRed(h.izq)  &&  isRed(h.der))     flipColors(h);
	        h.size = size(h.izq) + size(h.der) + 1;
		return h;
	}
	
	//get
	public Value get(Key key)
	{
		if(raiz==null) throw new IllegalArgumentException ("argumento es nulo");
		return get(raiz, key);
	}
	
	public Value get(Nodo x, Key key)
	{
		while(x!=null)
		{
			int cmp=key.compareTo(x.key);
			if(cmp<0) x=x.izq;
			else if(cmp>0) x=x.der;
			else
				return x.val;
		}
		return null;
	}
	
	public boolean contains(Key key)
	{
		return get(key)!=null;
	}
	
	//otras
    public int size() {
        return size(raiz);
    }
    
    public boolean isEmpty() {
        return raiz==null;
    }
	
    //Iterator
    public Iterator<Key> keys() {
        if (isEmpty()) return (new Cola<Key>()).iterator();
        return keys(min(), max());
    }
    
    public Iterator<Key> keys(Key lo, Key hi) {
        if (lo == null) throw new IllegalArgumentException("primer argumento en llaves es null");
        if (hi == null) throw new IllegalArgumentException("segundo argumento en llaves es null");

        Cola<Key> queue = new Cola<Key>();
        keys(raiz, queue, lo, hi);
        return  queue.iterator();
    } 

    private void keys(Nodo x, Cola<Key> queue, Key lo, Key hi) { 
        if (x == null) return; 
        int cmplo = lo.compareTo(x.key); 
        int cmphi = hi.compareTo(x.key); 
        if (cmplo < 0) keys(x.izq, queue, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key); 
        if (cmphi > 0) keys(x.der, queue, lo, hi); 
    } 
    

	public Iterator<Key> keysInRange(Key lo, Key hi) {
		if (lo == null)
			throw new IllegalArgumentException("Primer argumento keys() es null");

		if (hi == null)
			throw new IllegalArgumentException("Segundo argumento keys() es null");

		Cola<Key> queue = new Cola<Key>();
		keys(raiz, queue, lo, hi);
		return queue.iterator();
	} 


    public boolean check()
   {
    	return estaBalanceado() && isBST() && is23();
    }
    
    private boolean estaBalanceado()
    {
        // e. todas las ramas tienen el mismo n�mero de enlaces negros.
    	  int black = 0;     // number of black links on path from root to min
          Nodo x = raiz;
          while (x != null) {
              if (!isRed(x)) black++;
              x = x.izq;
          }
          return isBalanced(raiz, black);
    }
   
    private boolean isBalanced(Nodo x, int black) {
        if (x == null) return black == 0;
        if (!isRed(x)) black--;
        return isBalanced(x.izq, black) && isBalanced(x.der, black);
    } 
    
    private boolean isBST(Nodo x, Key min, Key max) {
    	//a.la llave de cada nodo sea mayor que cualquiera de su sub-�rbol izquierdo
    	//b.la llave de cada nodo sea menor que cualquiera de su sub�rbol derecho
        if (x == null) return true;
        if (min != null && x.key.compareTo(min) <= 0) return false;
        if (max != null && x.key.compareTo(max) >= 0) return false;
        return isBST(x.izq, min, x.key) && isBST(x.der, x.key, max);
    } 
    
    private boolean isBST() {
    	//a.la llave de cada nodo sea mayor que cualquiera de su sub-�rbol izquierdo
    	//b.la llave de cada nodo sea menor que cualquiera de su sub�rbol derecho
        return isBST(raiz, null, null);
    }
    
    private boolean is23() { return is23(raiz); }
    private boolean is23(Nodo x) {
        if (x == null) return true;
        if (isRed(x.der)) return false;
        if (x != raiz && isRed(x) && isRed(x.izq))
            return false;
        return is23(x.izq) && is23(x.der);
    } 
    
    public Key min()
    {
        if (isEmpty()) return null;
        else
        return min(raiz).key;
    }
    
    private Nodo min(Nodo x) { 
        // assert x != null;
        if (x.izq == null) return x; 
        else                return min(x.izq); 
    } 
    
    public Key max()
    {
        if (isEmpty()) return null;
        else
        return max(raiz).key;
    } 
    
    private Nodo max(Nodo x) { 
        // assert x != null;
        if (x.der == null) return x; 
        else                 return max(x.der); 
    }
    //helper functions
    
    private Nodo rotateRight(Nodo h)
    {
    	  Nodo x = h.izq;
          h.izq = x.der;
          x.der = h;
          x.color = x.der.color;
          x.der.color = RED;
          x.size = h.size;
          h.size = size(h.izq) + size(h.der) + 1;
          return x;
      }
    
    
    private Nodo rotateLeft(Nodo h) {
        // assert (h != null) && isRed(h.right);
        Nodo x = h.der;
        h.der = x.izq;
        x.izq = h;
        x.color = x.izq.color;
        x.izq.color = RED;
        x.size = h.size;
        h.size = size(h.izq) + size(h.der) + 1;
        return x;
    }
    
    private void flipColors(Nodo h) {
        h.color = !h.color;
        h.izq.color = !h.izq.color;
        h.der.color = !h.der.color;
    }
    
    private Nodo moveRedLeft(Nodo h) {

        flipColors(h);
        if (isRed(h.der.izq)) { 
            h.der = rotateRight(h.der);
            h = rotateLeft(h);
            flipColors(h);
        }
        return h;
    }


    private Nodo moveRedRight(Nodo h) {
    	flipColors(h);
        if (isRed(h.izq.izq)) { 
            h = rotateRight(h);
            flipColors(h);
        }
        return h;
    }
    
    private Nodo balance(Nodo h) {

        if (isRed(h.der))                      h = rotateLeft(h);
        if (isRed(h.izq) && isRed(h.izq.izq)) h = rotateRight(h);
        if (isRed(h.izq) && isRed(h.der))     flipColors(h);

        h.size = size(h.izq) + size(h.der) + 1;
        return h;
    }

    public int height() {
        return getheight(raiz);
    }
    public int getheight(Nodo x) {
        if (x == null) return -1;
        return 1 + Math.max(getheight(x.izq), getheight(x.der));
    }
    public Nodo darRaiz()
    {
    return raiz;
    }
    public int altura(Nodo x, int alt)
	{
		if(x==null)
		{
			return alt;
		}
		if(x!=null)
		{
			alt = alt + 1;
		}
		if(x.der==null && x.izq==null)
		{
			return alt;
		}
		return Math.max(altura(x.izq,alt), altura(x.der,alt));
	}
    
    public Iterator<Value> valuesInRange(Key lo, Key hi) {
		if (lo == null)
			throw new IllegalArgumentException("Primer argumento keys() es null");

		if (hi == null)
			throw new IllegalArgumentException("Segundo argumento keys() es null");

		Cola<Value> queue = new Cola<Value>();
		valuesInRange(raiz, queue, lo, hi);
		return queue.iterator();
	} 

	private void valuesInRange(Nodo x, Cola<Value> queue, Key lo, Key hi) { 
		if (x == null)
			return; 
		int cmplo = lo.compareTo(x.key); 
		int cmphi = hi.compareTo(x.key); 

		  if (cmplo < 0)
			  valuesInRange(x.izq, queue, lo, hi); 
			 if (cmplo <= 0 && cmphi >= 0)
					queue.enqueue(x.val); 
			if (cmphi > 0)
				valuesInRange(x.der, queue, lo, hi); 
		  
		 

	}
}

