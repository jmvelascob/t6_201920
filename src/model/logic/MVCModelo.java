package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.opencsv.CSVReader;

import model.data_structures.Cola;
import model.data_structures.RedBlackLiteBST;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	
	private Zona zona;
	private int contadorZonas;
	private String instruccion;
	private RedBlackLiteBST<Integer, Zona> arbol;
	
	public MVCModelo()
	{
		arbol= new RedBlackLiteBST<Integer, Zona>();
	}
	public void cargarJSON() 
	{
		try
		{
			JsonParser parser = new JsonParser();
			String path = "./data/bogota_cadastral.json";
	        FileReader fr = new FileReader(path);
	        JsonElement datos = parser.parse(fr);
	        json(datos);
		}
		catch(Exception e)
		{
			System.out.println("No se pudo leer el json");
		}
	}public void json(JsonElement elemento) {
	    if (elemento.isJsonObject()) {
	       // System.out.println("Es objeto");
	        JsonObject obj = elemento.getAsJsonObject();
	        java.util.Set<java.util.Map.Entry<String,JsonElement>> entradas = obj.entrySet();
	        java.util.Iterator<java.util.Map.Entry<String,JsonElement>> iter = entradas.iterator();
	        while (iter.hasNext()) 
	        {
	            java.util.Map.Entry<String,JsonElement> entrada = iter.next();
	           // System.out.println("Clave: " + entrada.getKey());
	            if(entrada.getKey().equals("coordinates"))
	            {
	            	instruccion = "coordinates";
	            	zona = new Zona();
	            	contadorZonas++;
	            }
	            else if(entrada.getKey().equals("type"))
	            {
	            	instruccion = "type";
	            }
	            else if(entrada.getKey().equals("geometry"))
	            {
	            	instruccion = "geometry";
	            }
	            else if(entrada.getKey().equals("cartodb_id"))
	            {
	            	instruccion = "cartodb_id";
	            }
	            else if(entrada.getKey().equals("scacodigo"))
	            {
	            	instruccion = "scacodigo";
	            }
	            else if(entrada.getKey().equals("scatipo"))
	            {
	            	instruccion = "scatipo";
	            }
	            else if(entrada.getKey().equals("scanombre"))
	            {
	            	instruccion = "scanombre";
	            }
	            else if(entrada.getKey().equals("shape_leng"))
	            {
	            	instruccion = "shape_leng";
	            }
	            else if(entrada.getKey().equals("shape_area"))
	            {
	            	instruccion = "shape_area";
	            }
	            else if(entrada.getKey().equals("MOVEMENT_ID"))
	            {
	            	instruccion = "MOVEMENT_ID";
	            }
	            else if(entrada.getKey().equals("DISPLAY_NAME"))
	            {
	            	instruccion = "DISPLAY_NAME";
	            }
	            else if(entrada.getKey().equals("properties"))
	            {
	            	instruccion="properties";
	            }
	            //System.out.println("Valor:");
	            json(entrada.getValue());
	        }
	 
	    } else if (elemento.isJsonArray()) {
	        JsonArray array = elemento.getAsJsonArray();
	        //System.out.println("Es array. Numero de elementos: " + array.size());
	        java.util.Iterator<JsonElement> iter = array.iterator();
	        while (iter.hasNext()) {
	            JsonElement entrada = iter.next();
	            json(entrada);
	        }
	    } else if (elemento.isJsonPrimitive()) {
	       // System.out.println("Es primitiva");
	        JsonPrimitive valor = elemento.getAsJsonPrimitive();
	        if (valor.isBoolean())
	        {
	           // System.out.println("Es booleano: " + valor.getAsBoolean());
	        } 
	        else if (valor.isNumber()) 
	        {	
	        	if(instruccion.equals("coordinates"))
	        	{
	        		agregarCoordenadas(valor.getAsDouble());
	        	}
	        	else if(instruccion.equals("MOVEMENT_ID"))
	        	{
	        		agregarMovementID(valor.getAsInt());
	        	}
	        	else if(instruccion.equals("shape_leng"))
	            {
	            	agregarShapeLen(valor.getAsDouble());
	            }
	            else if(instruccion.equals("shape_area"))
	            {
	            	agregarShapeArea(valor.getAsDouble());
	            }
	            //System.out.println("Es numero: " + valor.getAsNumber());
	            
	        } else if (valor.isString()) 
	        {
	        	if(instruccion.equals("scanombre"))
	        	{
	        		agregarSCANombre(valor.getAsString());
	        	}
	        	else if (instruccion.equals("MOVEMENT_ID"))
	        	{
	        		agregarMovementID(valor.getAsInt());
	        	}
	            //System.out.println("Es texto: " + valor.getAsString());
	        }
	    } else if (elemento.isJsonNull()) {
	        System.out.println("Es NULL");
	    } else {
	        System.out.println("Es otra cosa");
	    }
	}
	
	public void agregarCoordenadas(double valor)
	{
		NodoCoordenadas primer = zona.darPrimerasCoordenadas();
		while(primer.darSiguiente()!=null)
		{
			primer = primer.darSiguiente();
		}
		if(primer.darLongitud()==0)
		{
			primer.cambiarLongitud(valor);
		}
		else if(primer.darLatitud()==0)
		{
			primer.cambiarLatitud(valor);
			NodoCoordenadas nuevo = new NodoCoordenadas();
			primer.cambiarSiguiente(nuevo);
		}
	}
	
	public void agregarMovementID(int valor)
	{
		zona.cambiarMovementID(valor);
		arbol.put(zona.darMovementID(), zona);
	}
	
	public void agregarSCANombre(String valor)
	{
		zona.cambiarSCANombre(valor);
	}
	
	public void agregarShapeLen(double valor)
	{
		zona.cambiarShapeLen(valor);
	}
	
	public void agregarShapeArea(double valor)
	{
		zona.cambiarShape_Area(valor);
	}
	
	public int darNumeroZonas()
	{
		return contadorZonas;
	}
	
	public RedBlackLiteBST<Integer, Zona>  dararbol()
	{
		return arbol;
	}

	public void cargarTotal() 
	{
		// TODO Auto-generated method stub
		cargarJSON();
	}
	

	public Zona consultarZonaPorId(int mov)
	{
		Zona zon=arbol.get(mov);
		return zon;
	}
	

	public  Iterator<Zona> zonasEnElRango(int inf, int max)
	{
		Iterator<Zona> it= arbol.valuesInRange(inf, max);
		return it;
	}
	
	}
	
	
	

