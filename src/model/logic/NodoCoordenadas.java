package model.logic;

public class NodoCoordenadas 
{
	private double longitud;
	
	private double latitud;
	
	private NodoCoordenadas siguiente;
	
	public NodoCoordenadas()
	{
		longitud = 0;
		latitud = 0;
	}
	
	public double darLongitud()
	{
		return longitud;
	}
	
	public double darLatitud()
	{
		return latitud;
	}
	
	public void cambiarLongitud( double pLongitud)
	{
		longitud = pLongitud;
	}
	
	public void cambiarLatitud( double pLatitud)
	{
		latitud = pLatitud;
	}
	
	public void cambiarSiguiente(NodoCoordenadas sig)
	{
		siguiente = sig;
	}
	
	public NodoCoordenadas darSiguiente()
	{
		return siguiente;
	}

}
