package model.logic;

public class Zona implements Comparable<Zona>
{
	private int movementID;
	private String scaNombre;
	private double shape_Len;
	private double shape_Area;
	private NodoCoordenadas primeraCoordenada;
	
	public Zona()
	{
		primeraCoordenada = new NodoCoordenadas();
	}
	
	public String darnom()
	{
		return scaNombre;
	}
	
	public double darPer()
	{
		return shape_Len;
	}
	
	public double darArea()
	{
		return shape_Area;
	}
	public int darMovementID()
	{
		return movementID;
	}
	
	public void cambiarMovementID(int pm)
	{
		movementID = pm;
	}
	
	public void cambiarSCANombre(String nombre)
	{
		scaNombre = nombre;
	}
	
	public void cambiarShapeLen(double len)
	{
		shape_Len = len;
	}
	
	public void cambiarShape_Area(double len)
	{
		shape_Area = len;
	}
	
	public NodoCoordenadas darPrimerasCoordenadas()
	{
		return primeraCoordenada;
	}

	public int numnodos()
	{
		int cont=0;
		NodoCoordenadas no=primeraCoordenada;
		while(no!=null)
		{
			cont++;
			no=no.darSiguiente();
		}
		return cont;
	}
	@Override
	public int compareTo(Zona arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
