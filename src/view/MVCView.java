package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Cargar información");
			System.out.println("2. Dado un MOVEMENT_ID de una zona que ingresa el usuario se busca su información");
			System.out.println("3. Dado un rango de MOVEMENT_IDs (mov_id_inferior y mov_id_maximo) que ingresa el usuario se buscan todas las zonas en el rango.");
			System.out.println("6. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			// TODO implementar
		}
}
