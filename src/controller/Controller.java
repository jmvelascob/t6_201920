package controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

import model.logic.MVCModelo;
import model.logic.Zona;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				modelo.cargarJSON();
				System.out.println("El n�mero total de zonas "+modelo.darNumeroZonas());	
				System.out.println("Valor m�nimo: "+modelo.dararbol().min());
				System.out.println("Valor maximo: "+modelo.dararbol().max());
				System.out.println("Alt "+modelo.dararbol().height());
				System.out.println("Altura promedio: " +modelo.dararbol().altura(modelo.dararbol().darRaiz(), 0));

				break;

			case 2:
				System.out.println("Introducir MOVEMENT_ID");
				String dat= lector.next();
				Zona z=modelo.consultarZonaPorId(Integer.parseInt(dat));
				System.out.println("Nombre: "+z.darnom()+ ", Perimetro: " + z.darPer() + ", �rea: "+z.darArea() + ", N�mero de puntos: "+z.numnodos());

				break;

			case 3:
				System.out.println("Introducir MOVEMENT_ID m�nimo");
				String datt= lector.next();
				System.out.println("Introducir MOVEMENT_ID m�ximo");
				String dattt= lector.next();
				Iterator<Zona> it=modelo.zonasEnElRango(Integer.parseInt(datt),Integer.parseInt(dattt) );
				while(it.hasNext())
				{
					Zona l= it.next();	
				System.out.println("Nombre: "+l.darnom()+ ", Perimetro: " + l.darPer() + ", �rea: "+l.darArea()+ ", N�mero de puntos: " +l.numnodos());
				}
				break;
			


			default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
